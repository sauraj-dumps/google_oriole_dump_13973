## oriole-user 12 SP2A.220505.002 8353555 release-keys
- Manufacturer: google
- Platform: gs101
- Codename: oriole
- Brand: google
- Flavor: oriole-user
- Release Version: 12
- Id: SP2A.220505.002
- Incremental: 8353555
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/oriole/oriole:12/SP2A.220505.002/8353555:user/release-keys
- OTA version: 
- Branch: oriole-user-12-SP2A.220505.002-8353555-release-keys
- Repo: google_oriole_dump_13973


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
